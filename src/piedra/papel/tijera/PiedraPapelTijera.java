
package piedra.papel.tijera;


public class PiedraPapelTijera {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Player p1 = new Player();
        Player p2 = new Player();
        boolean gameWon = false;  
        int roundsPlayed = 0;    // Number of rounds played
        //INLINE TEMP: ------
        int draw = 0;
        /*String p1Choice;
        String p2Choice;*/
        // Game Loop
        do
        {
            System.out.println("***** Round: " +
                roundsPlayed + " *********************\n");
            System.out.println("Number of Draws: "+ 
                draw + "\n");
                //Extraer mètodos: ------
                
            String result1 = printPlayer( p1, 1);
            String result2 = printPlayer( p1, 2);
            int resultado = jugar(p1, p2, result1, result2);
            switch (resultado) {
                case 0:
                    draw++;            
                    System.out.println("\n\t\t\t Draw \n");
                    break;
                case 1:
                    p1.setWins();
                    System.out.println("Player 1 Wins");
                    break;
                case 2:
                    p2.setWins();       
                    System.out.println("Player 2 Wins");
                    break;
            }
            
            roundsPlayed++;
            if((p1.getWins()>=3)||(p2.getWins()>=3))
            {
                gameWon = true;
                System.out.println("GAME WON");
            }
            System.out.println();
        } while(gameWon != true);
        
        
    }
        public static String printPlayer(Player p, int cont){
            String result = p.playerChoice(); 
            System.out.println("Player " + cont + ": " + result + 
                "\t Player " + cont + " Total Wins: " + p.getWins() );
            return result;
        }
        
        public static int jugar(Player p1, Player p2,String result1, String result2){
            String[] jerarquia= {"rock", "scissors", "paper"};
            if (result1 == result2){
                    return 0;
            }else if ((result1== jerarquia[0])&&(result2== jerarquia[2])){
                    return 2;                     
                    
            }else if ((result1== jerarquia[1])&&(result2== jerarquia[0])){
                    return 2;                     
                     
            }else if ((result1== jerarquia[2])&&(result2== jerarquia[0])){
                    return 2;                     
                    
            }
            return 1;                
        }
}


    
    

